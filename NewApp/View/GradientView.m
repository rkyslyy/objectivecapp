//
//  GradientView.m
//  NewApp
//
//  Created by Roman Kyslyy on 12/9/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

#import "GradientView.h"

@implementation GradientView

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CAGradientLayer * gradient = [[CAGradientLayer alloc] init];
    [gradient setFrame:self.frame];
    [gradient setStartPoint:CGPointMake(0, 0)];
    [gradient setEndPoint:CGPointMake(1, 1)];
    
    UIColor * top = [UIColor colorWithRed:(CGFloat)66 / 255 green:(CGFloat)109 / 255 blue:(CGFloat)225 / 255 alpha:1];
    UIColor * bot = [UIColor colorWithRed:(CGFloat)49 / 255 green:(CGFloat)0 / 255 blue:(CGFloat)78 / 255 alpha:1];
    
    gradient.colors = @[(id)top.CGColor, (id)bot.CGColor];
    
    [[self layer] addSublayer:gradient];
}

@end
