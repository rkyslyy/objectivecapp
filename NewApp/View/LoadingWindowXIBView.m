//
//  LoadingWindowXIBView.m
//  NewApp
//
//  Created by Roman Kyslyy on 12/9/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

#import "LoadingWindowXIBView.h"

@interface LoadingWindowXIBView()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;


@end

@implementation LoadingWindowXIBView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void) customInit {
    [[NSBundle mainBundle] loadNibNamed:@"LoadingWindow" owner:self options:nil];
    
    [self addSubview:self.contentView];
    
    self.contentView.frame = self.bounds;
    [self.spinner startAnimating];
}

@end
