//
//  ViewController.m
//  NewApp
//
//  Created by Roman Kyslyy on 12/8/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

#import "../View/LoadingWindowXIBView.h"
#import "ViewController.h"
#import <AFNetworking.h>

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *loginBackgroundView;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) LoadingWindowXIBView *loadingView;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupLoginWindow];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void) removeLoad {
    [self.loadingView removeFromSuperview];
}

- (NSString*) generateLink : (NSDictionary*) parameters forURL : (NSString*) baseURL {
    NSString * result = @"?";
    
    for (id key in [parameters allKeys]) {
        NSString * strKey = key;
        result = [result stringByAppendingString:strKey];
        result = [result stringByAppendingString:@"="];
        result = [result stringByAppendingString:[parameters valueForKey:key]];
        result = [result stringByAppendingString:@"&"];
    }
    return [baseURL stringByAppendingString:result];
}

- (void) setupLoginWindow {
    _loginBackgroundView.layer.shadowRadius = 5;
    _loginBackgroundView.layer.shadowOpacity = 50;
    _usernameField.layer.borderColor = UIColor.clearColor.CGColor;
}

- (BOOL)prefersStatusBarHidden {
    return true;
}

- (void) hideKeyboard {
    [[self view] endEditing:true];
}

- (IBAction)loginPressed:(UIButton *)sender {
    NSDictionary * dicParameters = [NSDictionary dictionaryWithObjectsAndKeys:@"client_credentials", @"grant_type",
                                    @"08d991b8df416e6759d5a274a3eda67055f55cb6947b0f80558ef2a843212d4f", @"client_id",
                                    @"0515a9eed9e223a3559d9d1c0ef8a748278623e9c38ade8634d50558c6ddd082", @"client_secret",
                                    nil];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
    
    [request setHTTPMethod:@"POST"];
    [request setURL:[NSURL URLWithString:[self generateLink:dicParameters forURL:@"https://api.intra.42.fr/oauth/token"]]];
    
    self.loadingView = [[LoadingWindowXIBView alloc] initWithFrame:CGRectMake(10, 10, 200, 135)];
    [self.view addSubview:self.loadingView];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
          NSLog(@"Data received: %@", myString);
          [self performSelectorOnMainThread:@selector(removeLoad) withObject:nil waitUntilDone:false];
      }] resume];
}

@end
