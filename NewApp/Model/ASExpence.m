//
//  ASExpence.m
//  NewApp
//
//  Created by Roman Kyslyy on 12/9/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

#import "ASExpence.h"

@implementation ASExpence

- (instancetype) init : (ExpenceType) type amount : (double) amount {
    self = [super init];
    
    self.type = type;
    self.amount = amount;
    self.date = [NSDate date];
    
    return self;
}

@end
