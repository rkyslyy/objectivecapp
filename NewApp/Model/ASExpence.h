//
//  ASExpence.h
//  NewApp
//
//  Created by Roman Kyslyy on 12/9/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum    {
    Transport,
    Food,
    Movies,
    Gifts,
    Weed
}               ExpenceType;

@interface ASExpence : NSObject

@property (assign, nonatomic) ExpenceType   type;
@property (assign, nonatomic) double        amount;
@property (strong, nonatomic) NSDate *      date;

- (instancetype) init : (ExpenceType) type amount : (double) amount;

@end

NS_ASSUME_NONNULL_END
