//
//  ASUser.m
//  NewApp
//
//  Created by Roman Kyslyy on 12/9/18.
//  Copyright © 2018 Roman Kyslyy. All rights reserved.
//

#import "ASUser.h"

@implementation ASUser

- (instancetype)init:(NSString *)name {
    
    self = [super init];
    
    self.name = name;
    self.expences = [NSMutableArray array];
    
    return self;
}

@end
